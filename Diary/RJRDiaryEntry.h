//
//  THDiaryEntry.h
//  Diary
//
//  Created by Robert Randell on 07/05/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ENUM(int16_t, RJRDiaryEntryMood) {
    
    RJRDiaryEntryMoodGood = 0,
    RJRDiaryEntryMoodAverage = 1,
    RJRDiaryEntryMoodBad = 2
    
};

@interface RJRDiaryEntry : NSManagedObject

@property (nonatomic) NSTimeInterval date;
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSData * imageData;
@property (nonatomic) int16_t mood;
@property (nonatomic, retain) NSString * location;

@end
