//
//  THDiaryEntry.m
//  Diary
//
//  Created by Robert Randell on 07/05/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRDiaryEntry.h"


@implementation RJRDiaryEntry

@dynamic date;
@dynamic body;
@dynamic imageData;
@dynamic mood;
@dynamic location;

@end
