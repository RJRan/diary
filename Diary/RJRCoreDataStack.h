//
//  RJRCoreDataStack.h
//  Diary
//
//  Created by Robert Randell on 07/05/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RJRCoreDataStack : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (instancetype)defaultStack;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
